from django.urls import path
from.views import home, habitaciones, login, registrate

urlpatterns = [
    path('', home, name="home"),
    path('habitaciones/', habitaciones, name="habitaciones"),
    path('login/', login, name="login"),
    path('registrate', registrate, name="registrate"),
]