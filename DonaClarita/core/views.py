from django.shortcuts import render

# Create your views here.

def home(reques):
    return render(reques, 'core/index.html')

def habitaciones(request):
    return render(request, 'core/habitaciones.html')

def login(request):
    return render(request, 'core/login.html')

def registrate(request):
    return render(request, 'core/registrate.html')